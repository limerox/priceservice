package com.test.priceservice.services.impl;


import com.test.priceservice.currency.Currency;
import com.test.priceservice.dto.PriceDto;
import com.test.priceservice.entities.Price;
import com.test.priceservice.repository.PriceRepo;
import com.test.priceservice.services.PriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.Valid;

@Service
public class PriceServiceImpl implements PriceService {

    private PriceRepo priceRepo;

    @Autowired
    public PriceServiceImpl(PriceRepo priceRepo){
        this.priceRepo = priceRepo;
    }


    @Override
    public PriceDto getPrice(Integer productId) {

        Price price = priceRepo.findByProductId(productId);

        PriceDto priceDto = new PriceDto();
        priceDto.setPrice(price.getPrice()+" "+price.getCurrency().toString());
        priceDto.setProductId(price.getProductId());

        return priceDto;
    }

    @Override
    public PriceDto addPrice(@Valid PriceDto priceDto) {

        Price price = new Price();

        String[] priceArray = priceDto.getPrice().split(" ");

        price.setPrice(Double.valueOf(priceArray[0]));
        price.setCurrency(Currency.valueOf(priceArray[1]));
        price.setProductId(priceDto.getProductId());

        Price priceFromRepo = priceRepo.save(price);
        PriceDto priceDtoReturn = new PriceDto();
        priceDtoReturn.setProductId(priceFromRepo.getProductId());
        priceDtoReturn.setPrice(priceFromRepo.getPrice().toString()+" "+priceFromRepo.getCurrency());

        return priceDtoReturn;
    }
}
