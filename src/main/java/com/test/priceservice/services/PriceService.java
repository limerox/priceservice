package com.test.priceservice.services;

import com.test.priceservice.dto.PriceDto;

public interface PriceService {

    PriceDto getPrice(Integer productId);

    PriceDto addPrice(PriceDto priceDto);

}
