package com.test.priceservice.web.controller;


import com.test.priceservice.dto.PriceDto;
import com.test.priceservice.services.PriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/price")
public class PriceController {


    private PriceService priceService;

    @Autowired
    public PriceController(PriceService priceService){
        this.priceService = priceService;
    }

    @GetMapping
    public @ResponseBody PriceDto getPrice(@RequestParam int productId){
        return priceService.getPrice(productId);
    }

    @PostMapping
    public @ResponseBody PriceDto addPrice(@RequestBody PriceDto priceDto){
        return priceService.addPrice(priceDto);
    }



}
