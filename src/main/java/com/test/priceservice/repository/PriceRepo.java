package com.test.priceservice.repository;


import com.test.priceservice.entities.Price;
import org.springframework.data.repository.CrudRepository;


public interface PriceRepo extends CrudRepository<Price, Integer> {

        Price findByProductId(Integer productId);

}
