package com.test.priceservice.unit;


import com.test.priceservice.currency.Currency;
import com.test.priceservice.dto.PriceDto;
import com.test.priceservice.entities.Price;
import com.test.priceservice.repository.PriceRepo;
import com.test.priceservice.services.impl.PriceServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;


import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PriceServiceUnitTests {

    @Mock
    PriceRepo priceRepo;

    @InjectMocks
    PriceServiceImpl priceService;

    @Test
    public void getPrice(){

        Price price = new Price();
        price.setProductId(1);
        price.setCurrency(Currency.CHF);
        price.setPrice(10.0);

        //given
        when(priceRepo.findByProductId(1)).thenReturn(price);

        //when
        PriceDto priceDto = priceService.getPrice(1);

        //test
        Assertions.assertThat(priceDto.getProductId()).isEqualTo(1);
        Assertions.assertThat(priceDto.getPrice()).isEqualTo("10.0 CHF");
    }

}
